src/react-hook-form: for only react hook
  ---> simple use case to use react-hook-form

src/tinacms-form: for only tinacms form
  ---> simple use case how to use tinacms-editor into form

src/combine-react-hook-fom-tinacms-form: for react-hook-form + tinacms-form
   ---> check the onChange function and validate function 


src/react-hook-form-slate: for react-hook-form + slate
  ---> live example: `https://www.slatejs.org/examples/richtext`.
	---> licence: `https://github.com/ianstormtaylor/slate/blob/main/License.md`
	---> list of examples: `https://github.com/ianstormtaylor/slate#examples`.
	---> go to the link bellow to see code of rich-text: `https://github.com/ianstormtaylor/slate/blob/main/site/examples/richtext.tsx`.
	---> format: `https://docs.slatejs.org/api/nodes` check the Descendant entity.
	---> check the src/reaact-hook-form-slate/app.js to check implementing using slate with react-hook-form
	---> the problem that that i think we will have is how to validate the format beccause it's not a string
 
