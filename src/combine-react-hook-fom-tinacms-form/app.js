import React from 'react';
import { useForm } from "react-hook-form";
import { InlineForm  } from 'react-tinacms-inline';
import { InlineWysiwygt } from '../tinaccms-form/inline-wisgy';
import { useForm as useTinaCMSForm } from 'tinacms'
import ReactMarkdown from 'react-markdown'
import * as yup from "yup";

const validationSchema = yup.object({
  required: yup.string().required("Required"),
  content: yup.string().required("Required")
});

const useYupValidationResolver = validationSchema =>
  React.useCallback(
    async data => {
      try {
        const values = await validationSchema.validate(data, {
          abortEarly: false
        });

        return {
          values,
          errors: {}
        };
      } catch (errors) {
        return {
          values: {},
          errors: errors.inner.reduce(
            (allErrors, currentError) => ({
              ...allErrors,
              [currentError.path]: {
                type: currentError.type ?? "validation",
                message: currentError.message
              }
            }),
            {}
          )
        };
      }
    },
    [validationSchema]
  );
function App() {
  const resolver = useYupValidationResolver(validationSchema);
  const { handleSubmit, formState: { errors }, setValue } = useForm({ resolver });
  const [result, form] = useTinaCMSForm({
    fields: [],
    validate(values) {
      // todo: to validate all field into the tinacms form.
      console.warn('validate', values)
    },
    onChange({ values }) {
      for (const [key, value] of Object.entries(values)) {
        setValue(key, value)
      }
    },
  });
  const onSubmit = data => console.log('submiting form', data);
  console.warn('hook-form', errors);
  return (
    <div className="App">
      <form onSubmit={handleSubmit(onSubmit)}>
        <InlineForm form={form}>
          <InlineWysiwygt name="required" format="markdown">
            <ReactMarkdown children={result.required} />
          </InlineWysiwygt>
          <InlineWysiwygt name="content" format="markdown">
            <ReactMarkdown children={result.content} />
          </InlineWysiwygt>
        </InlineForm>
        <input type="submit" />
      </form>
    </div>
  );
}

export default App;

