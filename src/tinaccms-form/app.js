import React from 'react';
import { InlineForm  } from 'react-tinacms-inline';
import { InlineWysiwygt } from './inline-wisgy';
import { usePlugin, useForm as useTinaCMSForm } from 'tinacms'
import './App.css';
import ReactMarkdown from 'react-markdown'

const formConfig = {
  fields: [
    {
      name: "body",
      label: "First MarkDown",
      component: "text",
    },
    {
      name: "markdownBody",
      label: "First MarkDown",
      component: "markdown"
    }
  ],
  onChange(values) {
    console.warn('Submitting: ', values);
  },
}

function App() {
  const [result, form] = useTinaCMSForm(formConfig);
  usePlugin(form);
  // console.warn(result)
  return (
    <div className="App">
      <InlineForm form={form}>

        <InlineWysiwygt name="markdownBody" format="markdown" onChange={console.warn}>
          <ReactMarkdown children={result.markdownBody} />
        </InlineWysiwygt>

      </InlineForm>
    </div>
  );
}

export default App;
