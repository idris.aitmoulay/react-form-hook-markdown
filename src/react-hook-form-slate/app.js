import React from 'react';
import { useForm } from "react-hook-form";
import * as yup from "yup";
import MarkdownEditor from './slate-previous';

const validationSchema = yup.object({
  content: yup.array().required("Required")
});

const useYupValidationResolver = validationSchema =>
  React.useCallback(
    async data => {
      try {
        const values = await validationSchema.validate(data, {
          abortEarly: false
        });

        return {
          values,
          errors: {}
        };
      } catch (errors) {
        return {
          values: {},
          errors: errors.inner.reduce(
            (allErrors, currentError) => ({
              ...allErrors,
              [currentError.path]: {
                type: currentError.type ?? "validation",
                message: currentError.message
              }
            }),
            {}
          )
        };
      }
    },
    [validationSchema]
  );
function App() {
  const resolver = useYupValidationResolver(validationSchema);
  const { handleSubmit, formState: { errors }, setValue } = useForm({ resolver });
  const onSubmit = data => console.log('submiting form', data);
  console.warn('hook-form', errors);
  return (
    <div className="App">
      <form onSubmit={handleSubmit(onSubmit)}>
        <MarkdownEditor onChangeValue={values => setValue('content', values)}/>
        <input type="submit" />
      </form>
    </div>
  );
}

export default App;

