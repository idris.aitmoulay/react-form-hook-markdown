import React from 'react';
import TinacmsApp from './tinaccms-form/app'
import ReactHookApp from './react-hook-form/app'
import TinacmsHookFormApp from './combine-react-hook-fom-tinacms-form/app'
import SlateFormApp from './react-hook-form-slate/app'

function App() {
  return (
    <div className="App">
           <h1>TinaCms Form</h1>
      <TinacmsApp />
      <div></div>
      <h1>React Hook Form</h1>
      <ReactHookApp />
      <h1>TinaCms Form + React Hook Form</h1>
      <TinacmsHookFormApp />

      <h1>Slate + React Hook Form</h1>
      <SlateFormApp />
    </div>
  );
}

export default App;
