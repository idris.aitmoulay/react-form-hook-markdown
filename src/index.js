import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { TinaProvider, TinaCMS } from 'tinacms';
const cms = new TinaCMS({ sidebar: false, enabled: true });

import("react-tinacms-editor").then(
  ({ MarkdownFieldPlugin,  }) => {
    cms.plugins.add(MarkdownFieldPlugin);
  }
)



ReactDOM.render(
  <React.StrictMode>
    <TinaProvider cms={cms}>
      <App />
    </TinaProvider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
